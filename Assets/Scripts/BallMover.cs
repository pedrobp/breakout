﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody))]

public class BallMover : MonoBehaviour {
	
	public float maxVelocity {get; set;}
	
	
	void Awake() {
		maxVelocity = 25;
	}
	
	void Start () {
		GetComponent<Rigidbody>().AddForce(transform.TransformDirection (-Vector3.up));
	}
	
	void FixedUpdate () {
		this.GetComponent<Rigidbody>().velocity = this.GetComponent<Rigidbody>().velocity.normalized * maxVelocity;
		if(this.transform.position.y<-10) resetPosition();

		if (offScreenLeft () || offScreenRight ())
			this.GetComponent<Rigidbody> ().velocity = Vector3.Scale(this.GetComponent<Rigidbody> ().velocity, new Vector3 (-1, 1, 1)); 

		if (offScreenTop () )
			this.GetComponent<Rigidbody> ().velocity = Vector3.Scale(this.GetComponent<Rigidbody> ().velocity, new Vector3 (1, -1, 1)); 
	}
	
	private void resetPosition() {
		this.transform.position = new Vector3(0,3,0);
		GetComponent<Rigidbody>().AddForce(transform.TransformDirection (-Vector3.up));
	}

	private bool offScreenLeft() {
		Vector3 viewportPos = (Camera.main.WorldToViewportPoint(this.transform.position));
		if (viewportPos.x < 0)
			return true;
		else
			return false;
	}
	
	private bool offScreenRight() {
		Vector3 viewportPos = (Camera.main.WorldToViewportPoint(this.transform.position));
		if (viewportPos.x > 1)
			return true;
		else
			return false;
	}

	private bool offScreenTop() {
		Vector3 viewportPos = (Camera.main.WorldToViewportPoint(this.transform.position));
		if (viewportPos.y > 1)
			return true;
		else
			return false;
	}

	//added code 2
	public void makeBallFaster(float val) {
		maxVelocity += val;
		StartCoroutine (makeBallSlower (val, 2.0f));
	}

	private IEnumerator makeBallSlower(float val, float waitTime) {
		yield return new WaitForSeconds (waitTime);
		maxVelocity -= val;
	}
}
