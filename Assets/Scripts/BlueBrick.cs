﻿using UnityEngine;
using System.Collections;

public class BlueBrick : Brick {
	
	void Awake () {
		this.GetComponent<Brick> ().life = 3;
		this.GetComponent<Brick> ().Score = 100;
		this.GetComponent<Renderer> ().material.color = new Color (0, 0, 1, 1);
	}

	//added code 2
	override protected void OnMyBrickDestroyed() {
		gameManager.GetComponent<GameManager>().hideGreenBricks();
	}
}
