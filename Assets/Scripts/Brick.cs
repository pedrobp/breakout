﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

	public int life = 1;
	public int Score { get; set; }

	//added code
	protected GameObject gameManager;

	//added code 2
	protected virtual void OnMyBrickTriggerEnter(Collider obj) {
	}

	//added code
	protected virtual void OnMyBrickCollisionEnter(Collision obj) {
		Debug.Log ("Brick called this method");
	}

	//added code 2
	protected virtual void OnMyBrickDestroyed(){
	}

	void Awake () {
		Score = 10;
	}

	//added code
	void Start() {
		if (GameObject.Find ("GameManager") != null)
			gameManager = GameObject.Find ("GameManager");
	}

	void OnCollisionEnter(Collision obj) {

		//added code
		OnMyBrickCollisionEnter (obj);

		if (obj.gameObject.tag == "ball") {
			life--;
		}
		if (life == 0) {

			OnMyBrickDestroyed(); //added code 2

			Destroy (this.gameObject);
			//this line requires a change to GameManager.cs
			GameObject.Find ("GameManager").GetComponent<GameManager>().addScore(Score);
		}
	}

	//added code 2
	void OnTriggerEnter(Collider obj) {

		OnMyBrickTriggerEnter (obj);

		if (obj.gameObject.tag == "ball") {
			life--;
		}
		if (life == 0) {
			
			OnMyBrickDestroyed(); //added code 2
			
			Destroy (this.gameObject);
			//this line requires a change to GameManager.cs
			GameObject.Find ("GameManager").GetComponent<GameManager>().addScore(Score);
		}
	}
}
