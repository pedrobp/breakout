﻿using System.Collections;
using System.Collections.Generic;

public class BrickLayouts  {
	
	private static int[] level1 = new int[] {4,3,1,2,1,1,1,2,3,2,
		1,2,3,4,3,2,1,3,2,1,
		3,1,2,3,4,2,4,4,1,4
	};
	
	private static int[] level2 = new int[] {1,1,1,1,1,1,1,1,1,1,
		2,2,2,2,2,2,2,2,2,2,
		3,4,3,4,3,4,3,4,3,4
	};
	
	private static int[] level3 = new int[] {1,2,1,2,1,2,1,2,1,2,
		4,3,4,3,4,3,4,3,4,3,
		1,1,1,1,1,1,1,1,1,1,
		2,2,2,2,2,2,2,2,2,2
	};
	
	public static List<int[]> Levels = new List<int[]>{level1,level2,level3};
}
