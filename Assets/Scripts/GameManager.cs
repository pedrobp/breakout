﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour { 

	public GameObject brickPrefab;
	public int Level {get; set;}

	public Text scoreBox; //link the TEXT GameObject to this in the Inspector Panel
	public int Score {get; set;}

	//added code
	public GameObject paddle;
	public GameObject ball;

	void Awake() { 
		Level = 0;
	}

	void Start () { 
		buildLevel(Level);
	}

	private void buildLevel(int level) {
		int[] currentBrickLayout = BrickLayouts.Levels[level]; 
		int rows = currentBrickLayout.Length/10;
		int column = 0;
		for(int i = 0; i < currentBrickLayout.Length; i++) {
			GameObject brick = Instantiate (brickPrefab) as GameObject;
			/*
			 * some code to be added later ---------------
			*/
			switch (currentBrickLayout[i]){
			case 1:
				brick.AddComponent<RedBrick>();
				break;
			case 2:
				brick.AddComponent<YellowBrick>();
				break;
			case 3:
				brick.AddComponent<BlueBrick>();
				break;
			case 4:
				brick.AddComponent<GreenBrick>();
				break;
			}

			brick.transform.position = new Vector3 (13.5f - column*(brick.GetComponent<Renderer>().bounds.size.x+0.1f), 
			                                        12 - ((brick.GetComponent<Renderer>().bounds.size.y+0.1f) * rows),
			                                        0); 
			brick.transform.rotation = Quaternion.identity;
			column++;
			if (column==10) {
				column = 0;
				rows++; 
			}
		}
	}

	public void addScore(int score){
		Score += score;
		scoreBox.text = "Score: " + score;
	}

	//added code
	public void makePaddleBigger(float val) {
		paddle.GetComponent<PaddleMover> ().makePaddleBigger (val);
	}

	//added code 2
	public void makeBallFaster(float val){
		ball.GetComponent<BallMover> ().makeBallFaster (val);
	}

	//added code 2
	public void hideGreenBricks()
	{
		GreenBrick[] greenbricks = GameObject.FindObjectsOfType (typeof(GreenBrick)) as GreenBrick[];
		foreach (GreenBrick brick in greenbricks) {
			brick.GetComponent<Renderer>().enabled = false;
		}
		StartCoroutine (showGreenBricks (2.0f));
	}

	private IEnumerator showGreenBricks (float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		GreenBrick[] greenbricks = GameObject.FindObjectsOfType (typeof(GreenBrick)) as GreenBrick[];
		foreach (GreenBrick brick in greenbricks) {
			brick.GetComponent<Renderer> ().enabled = true;
		}
	}

	public void makeBricksTriggers() {
		Brick[] bricks = GameObject.FindObjectsOfType (typeof(Brick)) as Brick[];
		foreach (Brick brick in bricks) {
			brick.GetComponent<Collider>().isTrigger=true;
		}
		StartCoroutine (makeBricksColliders (5.0f));
	}

	private IEnumerator makeBricksColliders(float waitTime) {
		yield return new WaitForSeconds (waitTime);
		Brick[] bricks = GameObject.FindObjectsOfType (typeof(Brick)) as Brick[];
		foreach (Brick brick in bricks) {
			brick.GetComponent<Collider> ().isTrigger = false;
		}
	}


}