﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Brick))]

public class GreenBrick : MonoBehaviour {

	void Awake () {
		this.GetComponent<Brick> ().life = 4;
		this.GetComponent<Brick> ().Score = 100;
		this.GetComponent<Renderer> ().material.color = new Color (0.05f, 1f, 0.02f);
	}

	void OnCollisionEnter(Collision obj) {
		if (obj.gameObject.tag == "ball" && this.GetComponent<Brick> ().life == 0) {
			GameObject.Find ("GameManager").GetComponent<GameManager> ().makeBricksTriggers ();
		}
	}
}
