﻿using UnityEngine;
using System.Collections;

public class PaddleMover : MonoBehaviour {
	
	private float moveX = 25f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.LeftArrow) && !offScreenLeft()) this.transform.position += new Vector3(-moveX*Time.deltaTime,0,0);
		if(Input.GetKey(KeyCode.RightArrow) && !offScreenRight()) this.transform.position += new Vector3(moveX*Time.deltaTime,0,0);
	}

	private bool offScreenLeft() {
		Vector3 viewportPos = (Camera.main.WorldToViewportPoint(this.transform.position));
		if (viewportPos.x < 0)
			return true;
		else
			return false;
	}

	private bool offScreenRight() {
		Vector3 viewportPos = (Camera.main.WorldToViewportPoint(this.transform.position));
		if (viewportPos.x > 1)
			return true;
		else
			return false;
	}

	//added code
	public void makePaddleBigger(float val){
		this.transform.localScale += new Vector3 (0, val, 0);
		StartCoroutine (resetScale (val, 2.0f));
	}

	//added code
	private IEnumerator resetScale(float val, float waitTime) {
		yield return new WaitForSeconds (waitTime);
		this.transform.localScale -= new Vector3 (0, val, 0);
	}
}
