﻿using UnityEngine;
using System.Collections;

public class RedBrick : Brick {

	void Awake () {
		life = 2;
		Score = 20;
		this.GetComponent<Renderer> ().material.color = new Color (1, 0, 0);
	}

	//added code
	override protected void OnMyBrickCollisionEnter(Collision obj) {
		if(obj.gameObject.tag == "ball" ) {
			gameManager.GetComponent<GameManager>().makePaddleBigger(0.5f);
		}
	}
}

