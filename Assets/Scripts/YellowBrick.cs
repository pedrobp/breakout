﻿using UnityEngine;
using System.Collections;

public class YellowBrick : Brick {
	
	void Awake () {
		life = 1;
		Score = 25;
		this.GetComponent<Renderer> ().material.color = new Color (1, 0.85f, 0.2f);
	}

	override protected void OnMyBrickCollisionEnter(Collision obj) {
		if (obj.gameObject.tag == "ball") {
			gameManager.GetComponent<GameManager>().makeBallFaster(5.0f);
		}
	}
}
